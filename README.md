React native 0.64.0
react-navigation/native: 5.9.2
Template: Typescript

```
    npm i cli-template-rn -g
```

**Usage**

```
    rn-template-ts init <projectName>
```
