import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import * as React from 'react';
import { setTopLevelNavigator } from './NavigationServices';
import { StackConfig, StackScreens } from './StackScreens';

export interface RouteProps {
}

const Stack = createStackNavigator();

export default class Route extends React.PureComponent<RouteProps, any> {
    constructor(props: RouteProps) {
        super(props);
    }

    public render() {
        return (
            <NavigationContainer ref={(navigation) => setTopLevelNavigator(navigation)}>
                <Stack.Navigator screenOptions={{headerShown: false}}>
                    {StackScreens.map((screens: StackConfig, index: number) =>
                        <Stack.Screen
                            name={screens.name}
                            component={screens.componet}
                            options={screens.options}
                            children={screens.children}
                            getComponent={screens.getComponent}
                            getId={screens.getId}
                            initialParams={screens.initialParams}
                            key={screens.key || index}
                            listeners={screens.listeners}
                        />
                    )}
                </Stack.Navigator>
            </NavigationContainer>
        );
    }
}
