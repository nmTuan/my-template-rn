import { CommonActions, StackActions, TabActions } from "@react-navigation/native";

let _navigator: any;

function setTopLevelNavigator(navigatorRef: any) {
    _navigator = navigatorRef;
}

function navigate(name: any, params?: any) {
    _navigator.dispatch(
        CommonActions.navigate({
            name,
            params,
        })
    );
};

function reset(name: any, params?: any) {
    _navigator.dispatch(
        CommonActions.reset({
            index: 0,
            routes: [{
                name,
                params
            }]
        })
    )
}

function pop() {
    _navigator.dispatch(CommonActions.goBack());
}

function goBack(key?: string) {
    _navigator.dispatch((state: any) => {
        return {
            ...CommonActions.goBack(),
            source: key,
            target: state.key,
        }
    });
}

function popMany(number: number) {
    _navigator.dispatch(
        StackActions.pop(number)
    );
}

function popToTop() {
    _navigator.dispatch(
        StackActions.popToTop()
    );
}

function replace(routeName: any, params: any) {
    _navigator.dispatch(
        StackActions.replace(routeName, params)
    );
}

function getCurrentRoute() {

    let route = _navigator.state.nav;
    while (route.routes) {
        route = route.routes[route.index];
    }
    return route;
}

function push(routeName: any, params: any = {}) {
    _navigator.dispatch(
        StackActions.push(routeName, params),
    );
}

function jumpTo(routeName: any, params: any = {}) {
    _navigator.dispatch(TabActions.jumpTo(routeName, params));
}

export {
    setTopLevelNavigator,
    navigate,
    reset,
    pop,
    popMany,
    popToTop,
    replace,
    getCurrentRoute,
    goBack,
    push,
    jumpTo
}