import { StackNavigationOptions } from "@react-navigation/stack";
import Detail from "features/detail";
import Detail2 from "features/detail2";
import Home from "features/home";
import * as ScreenName from "navigation/NameScreens";
import TabNavigation from "./TabNavigation";

export interface StackConfig {
    name: string;
    componet: React.ComponentType<any>;
    options?: StackNavigationOptions;
    children?: undefined;
    getComponent?: undefined;
    key?: string | number | null | undefined;
    initialParams?: object | undefined;
    getId?: (({ params }: {
        params: object | undefined;
    }) => string | undefined) | undefined;
    listeners?: any;
}

export const StackScreens: StackConfig[] = [
    { name: ScreenName.TabNavigation, componet: TabNavigation },
    { name: ScreenName.DetailScreen2, componet: Detail2 },
]