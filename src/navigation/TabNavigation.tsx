import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import svg from 'assets/svg';
import Detail from 'features/detail';
import Home from 'features/home';
import React from 'react';
import {SvgXml} from 'react-native-svg';
import {DetailScreen, HomeScreen} from './NameScreens';

const Tab = createBottomTabNavigator();
export default function TabNavigation() {
  return (
    <Tab.Navigator
      screenOptions={({route, navigation}) => ({
        tabBarIcon: ({focused}) => {
          let iconSvg;
          switch (route.name) {
            case HomeScreen:
              iconSvg = focused ? svg.tab.homeActive : svg.tab.homeInactive;
              break;
            case DetailScreen:
              iconSvg = focused ? svg.tab.homeActive : svg.tab.homeInactive;
              break;
            default:
              iconSvg = '';
              break;
          }
          return <SvgXml xml={iconSvg} width={20} height={20} />;
        },
      })}
      tabBarOptions={{
        activeTintColor: '#48fd48',
        inactiveTintColor: '#b6b8b6',
        keyboardHidesTabBar: true,
      }}>
      <Tab.Screen name={HomeScreen} component={Home} />
      <Tab.Screen name={DetailScreen} component={Detail} />
    </Tab.Navigator>
  );
}
