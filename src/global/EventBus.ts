import { Subject } from "rxjs";

export interface EventBusType {
    type: EventBusName;
    payload?: any;
}

export enum EventBusName {
    REFRESH_NETWORK = 'REFRESH_NETWORK',
}

export default class EventBus {
    private static instance: EventBus;
    private eventSubject = new Subject<EventBusType>();

    static getInstace(): EventBus {
        if(!EventBus.instance) {
            EventBus.instance = new EventBus();
        }
        return EventBus.instance;
    }

    get event() {
        return this.eventSubject.asObservable();
    };

    post(event: EventBusType) {
        return this.eventSubject.next(event);
    }
}