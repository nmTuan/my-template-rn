export default {
    countryCode: 'vi',
    currencyCode: 'VND',
    networkDisconnected: 'Không có kết nối Internet',
    checkConnection: 'Vui lòng kiểm tra lại đường truyền',
    refreshPage: 'Tải lại trang',
    /** CẢNH BÁO: KHÔNG THAY ĐỔI NHỮNG DÒNG TRÊN */
}