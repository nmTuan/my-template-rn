export default {
    countryCode: 'en',
    currencyCode: 'GBP',
    networkDisconnected: 'Internet disconnected',
    checkConnection: 'Please check your connection again',
    refreshPage: 'Reload'
    /** WARNING: DO NOT CHANGE THE LINE ABOVE */
}