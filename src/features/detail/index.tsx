import { DetailScreen, DetailScreen2, HomeScreen } from 'navigation/NameScreens';
import React from 'react';
import { Button, View } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
export interface DetailProps {
  navigation: any;
}

class Detail extends React.PureComponent<DetailProps, any> {
  constructor(props: DetailProps) {
    super(props);
  }

  public render() {
    const { navigation } = this.props;
    return (
      <SafeAreaView>
        <Button
          title="Go to Details2"
          onPress={() => navigation.navigate(DetailScreen2)}
        />

        <Button title="Go to Home" onPress={() => navigation.navigate(HomeScreen)} />
        <Button title="Go back" onPress={() => navigation.goBack()} />
        <Button
          title="Go back to first screen in stack"
          onPress={() => navigation.popToTop()}
        />
      </SafeAreaView>
    );
  }
}

export default Detail;
