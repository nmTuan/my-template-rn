import EventBus, { EventBusName, EventBusType } from 'global/EventBus';
import { DetailScreen, DetailScreen2 } from 'navigation/NameScreens';
import { navigate } from 'navigation/NavigationServices';
import React from 'react';
import { Button, SafeAreaView, Text } from 'react-native';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { Subscription } from 'rxjs';
import { decrease, increase } from 'stores/actions';
import { StoreState } from 'stores/reducers';

export interface HomeProps {
    navigation: any;
    count: number;
    increase: (num: number) => void;
    decrease: (num: number) => void;
}
class Home extends React.PureComponent<HomeProps, any> {
    private subcription = new Subscription();

    constructor(props: HomeProps) {
        super(props);
    }

    componentDidMount() {
        this.listenEventBus();
    }

    componentWillUnmount() {
        this.subcription.unsubscribe();
    }

    private listenEventBus = (): void => {
        this.subcription.add(
            EventBus.getInstace().event.subscribe((event: EventBusType) => {
                switch (event.type) {
                    case EventBusName.REFRESH_NETWORK:
                        return;
                    default:
                        return;
                }
            })
        )
    }

    public render() {
        const { navigation, count, increase, decrease } = this.props;
        return (
            <SafeAreaView>
                <Button title="navigate 2" onPress={() => navigate(DetailScreen2)} />
                <Button title="navigate" onPress={() => navigate(DetailScreen)} />
                <Text>{count}</Text>
                <Button title="increase" onPress={() => increase(1)} />
                <Button title="decrease" onPress={() => decrease(1)} />
            </SafeAreaView>
        );
    }
}

const mapPropsToState = (state: StoreState) => {
    return {
        count: state.counter.count
    }
}

const mapDispatchToProps = (dispatch: Dispatch) => ({
    increase: (num: number) => dispatch(increase(num)),
    decrease: (num: number) => dispatch(decrease(num))
})

export default connect(mapPropsToState, mapDispatchToProps)(Home);
