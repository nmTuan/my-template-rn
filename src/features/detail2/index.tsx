import { DetailScreen, HomeScreen } from 'navigation/NameScreens';
import React from 'react';
import { Button, SafeAreaView } from 'react-native';
export interface DetailProps {
  navigation: any;
}

class Detail2 extends React.PureComponent<DetailProps, any> {
  constructor(props: DetailProps) {
    super(props);
  }

  public render() {
    const { navigation } = this.props;
    return (
      <SafeAreaView>
        <Button
          title="Go to Details... again"
          onPress={() => navigation.navigate(DetailScreen)}
        />

        <Button title="Go to Home" onPress={() => navigation.navigate(HomeScreen)} />
        <Button title="Go back" onPress={() => navigation.goBack()} />
        <Button
          title="Go back to first screen in stack"
          onPress={() => navigation.popToTop()}
        />
      </SafeAreaView>
    );
  }
}

export default Detail2;
