import EventBus, { EventBusType } from 'global/EventBus';
import * as React from 'react';
import { View } from 'react-native';
import { Subscription } from 'rxjs';
import { setI18nConfig } from 'utils/translate';
import NetworkComponent from '../component/networking';

export interface RootProps {
}

export interface RootState {
}

export default class RootComponent extends React.PureComponent<RootProps, RootState> {
    private subcription = new Subscription();

    constructor(props: RootProps) {
        super(props);
        setI18nConfig();
        this.state = {
        };
    }

    componentDidMount() {
        this.listenEventBus();
    }

    componentWillUnmount() {
        this.subcription.unsubscribe();
    }

    private listenEventBus = ():void => {
        this.subcription.add(
            EventBus.getInstace().event.subscribe((event: EventBusType) => {
                
            })
        )
    }

    public render() {
        return (
            <View style={{flex: 1}}>
                {this.props.children}
                <NetworkComponent />
            </View>
        );
    }
}
