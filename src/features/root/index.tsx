import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { StoreState } from 'stores/reducers';
import RootComponent from './view/root';

const mapPropsToState = (state: StoreState) => ({});

const mapDispatchToProps = (dispatch: Dispatch) => ({});

export default connect(mapPropsToState, mapDispatchToProps)(RootComponent);
