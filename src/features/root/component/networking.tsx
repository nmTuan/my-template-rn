import NetInfo from '@react-native-community/netinfo';
import svg from 'assets/svg';
import * as React from 'react';
import {Button, Pressable, StyleSheet, Text, View} from 'react-native';
import {SvgXml} from 'react-native-svg';
import {translate} from 'utils/translate';
export interface NetworkProps {}

interface State {
  isConnected: boolean;
}

export default class NetworkComponent extends React.Component<
  NetworkProps,
  State
> {
  constructor(props: NetworkProps) {
    super(props);
    this.state = {
      isConnected: true,
    };
  }

  componentDidMount() {
    NetInfo.addEventListener((state) => {
      this.handleConnectivityChange(state.isConnected);
    });
  }

  handleConnectivityChange = (isConnected: boolean) => {
    if (this.state.isConnected != isConnected) {
      this.setState({isConnected});
    }
  };

  onRefresh = () => {
    NetInfo.fetch().then((state) => {
      this.handleConnectivityChange(state.isConnected);
    });
  };

  public render() {
    const {isConnected} = this.state;
    if (isConnected) return null;
    return (
      <View style={styles.container}>
        <SvgXml xml={svg.newworkDisconnected} height={200} />
        <Text style={styles.textStateNetwork}>
          {translate('networkDisconnected')}
        </Text>
        <Text style={styles.textCheckConnection}>
          {translate('checkConnection')}
        </Text>
        <Pressable style={styles.btnRefresh} onPress={this.onRefresh}>
          <Text style={styles.txtRefresh}>{translate('refreshPage')}</Text>
        </Pressable>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  txtRefresh: {color: '#ffffff'},
  btnRefresh: {
    backgroundColor: '#6fa8f2',
    paddingHorizontal: 10,
    paddingVertical: 5,
    borderRadius: 8,
    marginTop: 20,
  },
  textStateNetwork: {
    fontSize: 20,
    color: 'gray',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  textCheckConnection: {
    fontSize: 18,
    color: 'gray',
    textAlign: 'center',
    marginTop: 10,
  },
  container: {
    width: '100%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 10,
  },
});
