import _ from 'lodash';
import I18n from 'i18n-js';
import vi from 'language/vi';
import en from 'language/en';
import {I18nManager, NativeModules, Platform} from 'react-native';

const translationGetter: any = {
  vi_Vn: () => vi,
  en: () => en,
};

export function getLocale(): string {
  return Platform.OS === 'ios'
    ? NativeModules.SettingsManager.settings.AppleLanguages[0]
    : NativeModules.I18nManager.localeIdentifier;
}

export const translate = _.memoize(
  (key: string, config?: any) => {
    return I18n.t(key, config);
  },
  (key: string, config?: any) => (config ? key + JSON.stringify(config) : key),
);

export function setI18nConfig(localeCustom?: string, isRTL: boolean = false) {
  I18nManager.forceRTL(isRTL);
  const locale =
    localeCustom ||
    Object.keys(translationGetter).find(
      (item) => item.toUpperCase() === getLocale().toUpperCase(),
    ) ||
    'vi_Vn';
  I18n.translations[locale] = translationGetter[locale]();
  I18n.locale = locale;
}
