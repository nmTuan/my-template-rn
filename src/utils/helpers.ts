import {translate} from './translate';
import {Platform, StatusBar, Dimensions} from 'react-native';
const X_WIDTH = 375;
const X_HEIGHT = 812;

const XSMAX_WIDTH = 414;
const XSMAX_HEIGHT = 896;

const IP12_WIDTH = 390;
const IP12_HEIGHT = 844;

const IP12ProMax_WIDTH = 428;
const IP12ProMax_HEIGHT = 926;

const {height, width} = Dimensions.get('window');

export const isIPhoneX = () =>
  Platform.OS === 'ios' && !Platform.isPad && !Platform.isTVOS
    ? (width === X_WIDTH && height === X_HEIGHT) ||
      (width === XSMAX_WIDTH && height === XSMAX_HEIGHT)
    : false;

export const isIPhone12 = () =>
  Platform.OS === 'ios' && !Platform.isPad && !Platform.isTVOS
    ? (width === IP12_WIDTH && height === IP12_HEIGHT) ||
      (width === IP12ProMax_WIDTH && height === IP12ProMax_HEIGHT)
    : false;

export const StatusBarHeight = Platform.select({
  ios: isIPhone12() ? 47 : isIPhoneX() ? 44 : 20,
  android: StatusBar.currentHeight,
  default: 0,
});

export function formatCurrency(value: number | string): string {
  const currency = new Intl.NumberFormat(translate('countryCode'), {
    style: 'currency',
    currency: translate('currencyCode'),
  }).format(Number(value));
  return currency;
}
