export * from './Counter.action';

export const INCREASE = 'INCREASE';
export const DECREASE = 'DECREASE';