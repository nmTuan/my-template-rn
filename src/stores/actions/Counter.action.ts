import { ActionType } from "stores/type";
import { DECREASE, INCREASE } from ".";

export function increase(num: number): ActionType {
    return {
        type: INCREASE,
        payload: num,
    }
}

export function decrease(num: number): ActionType {
    return {
        type: DECREASE,
        payload: num,
    }
}