import { applyMiddleware, createStore } from "redux";
import { createLogger } from 'redux-logger';
import rootReducers from "./reducers";

export default function configureStore(sagaMiddleware: any) {
    if(__DEV__) {
        return createStore(rootReducers, applyMiddleware(sagaMiddleware, createLogger()));
    }
    return createStore(rootReducers, applyMiddleware(sagaMiddleware));
}