import { DECREASE, INCREASE } from "stores/actions";
import { ActionType } from "stores/type";

const initState = {
    count: 0
};

export function Counter(state = initState, action: ActionType) {
    switch (action.type) {
        case INCREASE:
            return { count: state.count + action.payload };
        case DECREASE:
            return { count: state.count - action.payload };
        default:
            return { ...state };
    }
}

