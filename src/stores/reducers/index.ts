import { combineReducers } from "redux";
import { Counter } from "./counter.reducer";

const rootReducers = combineReducers({
    counter: Counter,
})

export default rootReducers;
export type StoreState = ReturnType<typeof rootReducers>;