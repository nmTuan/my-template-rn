
import RootComponent from 'features/root/view/root';
import Route from 'navigation/Route';
import React from 'react';
import { Provider } from 'react-redux';
import createSagaMiddleware from 'redux-saga';
import configureStore from 'stores';

export interface AppProps {
}

const sagaMiddleware = createSagaMiddleware();
const store = configureStore(sagaMiddleware);
class App extends React.PureComponent<AppProps, any> {
  constructor(props: AppProps) {
    super(props);
  }

  public render() {
    return (
      <Provider store={store}>
        <RootComponent>
          <Route />
        </RootComponent>
      </Provider>

    );
  }
}

export default App;